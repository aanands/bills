class BillModel {
  double? totalBills;
  double? unpaid;
  double? overdue;
  double? dueSevenDays;
  double? dueThirtyDays;
  double? dueMoreThanThrityDays;
  String? errorMsg;

  BillModel.fromJson(Map<String, dynamic> json) {
    unpaid = json['data']['total_unpaid'];
    overdue = json['data']['total_overdue'];
    dueSevenDays = json['data']['total_overdue_seven_days'];
    dueThirtyDays = json['data']['total_overdue_thirty_days'];
    dueMoreThanThrityDays = json['data']['total_overdue_plus_days'];
    totalBills = unpaid! +
        overdue! +
        dueSevenDays! +
        dueThirtyDays! +
        dueMoreThanThrityDays!;
  }

  BillModel.withError(String error) : errorMsg = error;
}
