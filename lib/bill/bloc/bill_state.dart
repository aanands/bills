import 'package:bills/bill/model/bill_model.dart';

abstract class BillState {}

class BillInitial extends BillState {}

class BillLoading extends BillState {}

class BillLoaded extends BillState {
  final BillModel billModel;

  BillLoaded(this.billModel);
}

class BillError extends BillState {
  final String error;

  BillError(this.error);
}
