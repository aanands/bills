import 'bill_event.dart';
import 'bill_state.dart';
import '../model/bill_model.dart';
import '../api/bill_api.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class BillBloc extends Bloc<BillEvent, BillState> {
  BillBloc() : super(BillInitial()) {
    // implement on fetchbilldata function
    on<FetchBillData>((event, emit) async {
      emit(BillLoading());
      // try {
      final BillModel billModel = await BillApiProvider().fetchBill();
      emit(BillLoaded(billModel));
      // } catch (e) {
      //   emit(BillError("Failed to fetch data"));
      // }
    });
  }
}
