import 'package:bills/bill/bloc/bill_bloc.dart';
import 'package:bills/bill/bloc/bill_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'bloc/bill_event.dart';

class BillScreen extends StatefulWidget {
  const BillScreen({super.key});

  @override
  State<BillScreen> createState() => _BillScreenState();
}

class _BillScreenState extends State<BillScreen> {
  final BillBloc billBloc = BillBloc();

  @override
  void initState() {
    billBloc.add(FetchBillData());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Center(
          child: Text('Bills'),
        ),
      ),
      body: BlocBuilder<BillBloc, BillState>(
        bloc: billBloc,
        builder: (context, state) {
          if (state is BillLoaded) {
            return SingleChildScrollView(
              child: SafeArea(
                child: Padding(
                  padding: const EdgeInsets.only(
                    left: 25,
                    right: 20,
                    top: 20,
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text(
                        "All Bills",
                        style: TextStyle(
                          fontSize: 24,
                          fontWeight: FontWeight.bold,
                          color: Colors.blue,
                        ),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      Text(
                        state.billModel.totalBills.toString(),
                        style: const TextStyle(
                          fontSize: 28,
                          fontWeight: FontWeight.w900,
                          color: Colors.black,
                        ),
                      ),
                      const Divider(
                        height: 40,
                        thickness: 9,
                        color: Colors.black,
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      const Text(
                        "Unpaid",
                        style: TextStyle(
                          fontSize: 22,
                          fontWeight: FontWeight.bold,
                          color: Colors.blue,
                        ),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      Text(
                        state.billModel.unpaid.toString(),
                        style: const TextStyle(
                          fontSize: 26,
                          fontWeight: FontWeight.w600,
                          color: Colors.black,
                        ),
                      ),
                      const Divider(
                        thickness: 2,
                        height: 40,
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      const Text(
                        "Overdue",
                        style: TextStyle(
                          fontSize: 22,
                          fontWeight: FontWeight.bold,
                          color: Colors.red,
                        ),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      Text(
                        state.billModel.overdue.toString(),
                        style: const TextStyle(
                          fontSize: 26,
                          fontWeight: FontWeight.w400,
                          color: Colors.black,
                        ),
                      ),
                      const Divider(
                        thickness: 2,
                        height: 40,
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      const Text(
                        "Due 7 days",
                        style: TextStyle(
                          fontSize: 22,
                          fontWeight: FontWeight.bold,
                          color: Colors.blue,
                        ),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      Text(
                        state.billModel.dueSevenDays.toString(),
                        style: const TextStyle(
                          fontSize: 26,
                          fontWeight: FontWeight.w400,
                          color: Colors.black,
                        ),
                      ),
                      const Divider(
                        thickness: 2,
                        height: 40,
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      const Text(
                        "Due 30 days",
                        style: TextStyle(
                          fontSize: 22,
                          fontWeight: FontWeight.bold,
                          color: Colors.blue,
                        ),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      Text(
                        state.billModel.dueThirtyDays.toString(),
                        style: const TextStyle(
                          fontSize: 26,
                          fontWeight: FontWeight.w400,
                          color: Colors.black,
                        ),
                      ),
                      const Divider(
                        thickness: 2,
                        height: 40,
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      const Text(
                        "Due 30+ days",
                        style: TextStyle(
                          fontSize: 22,
                          fontWeight: FontWeight.bold,
                          color: Colors.blue,
                        ),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      Text(
                        state.billModel.dueMoreThanThrityDays.toString(),
                        style: const TextStyle(
                          fontSize: 26,
                          fontWeight: FontWeight.w400,
                          color: Colors.black,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            );
          } else if (state is BillError) {
            return Center(
              child: Text(state.error),
            );
          }
          return const Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
    );
  }
}
