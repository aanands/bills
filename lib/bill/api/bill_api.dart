import 'package:bills/bill/model/bill_model.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

String companyId = "5108346617069701";

String baseURL = "https://d1who55cbx0x6d.cloudfront.net/v1/$companyId";

class BillApiProvider {
  Future<BillModel> fetchBill() async {
    dynamic res;
    try {
      res = await http.get(Uri.parse("$baseURL/get_bill_insights")).timeout(
            const Duration(seconds: 5),
          );
    } catch (e) {
      return BillModel.withError("Failed to fetch data");
    }
    if (res.statusCode == 200) {
      return BillModel.fromJson(json.decode(res.body));
    }
    return BillModel.withError("Failed to fetch data");
  }
}
